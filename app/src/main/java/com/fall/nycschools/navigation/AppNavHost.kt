package com.fall.nycschools.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.fall.nycschools.ui.screens.home.HomeScreen
import com.fall.nycschools.ui.screens.school.ListSchoolsScreen
import com.fall.nycschools.ui.screens.school.SchoolDetailsScreen
import com.fall.nycschools.viewModels.SchoolViewModel

@Composable
fun AppNavHost(viewModel: SchoolViewModel = hiltViewModel()) {
    val navController: NavHostController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = NavScreens.HomeScreen.route
    ) {

        composable(route = NavScreens.HomeScreen.route) {
            HomeScreen(navController = navController)
        }

        composable(route = NavScreens.ListSchoolsScreen.route) {
            LaunchedEffect(Unit) {
                viewModel.getSchools()
            }
            val schoolUiState = viewModel.schoolUiState.collectAsState().value
            ListSchoolsScreen(navController = navController, schoolUiState) { school ->
                viewModel.saveSelectedSchool(school = school)
                navController.navigate(NavScreens.SchoolDetailsScreen.route) {
                    launchSingleTop = true
                }
            }
        }

        composable(route = NavScreens.SchoolDetailsScreen.route) {
            val school = viewModel.selectedSchool
            LaunchedEffect(Unit) {
                school?.dbn?.let { viewModel.getScore(dbn = it) }
            }
            val scoreUiState = viewModel.scoreUiState.collectAsState().value
            SchoolDetailsScreen(
                navController = navController,
                scoreUiState = scoreUiState,
                school = school
            )
        }
    }
}
