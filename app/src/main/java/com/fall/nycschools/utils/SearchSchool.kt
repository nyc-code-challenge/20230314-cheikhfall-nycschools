package com.fall.nycschools.utils

import com.fall.nycschools.data.models.School

fun List<School>.searchSchool(key: String): List<School> {
    val tempList = mutableListOf<School>()
    this.forEach { school ->
        if (school.schoolName.lowercase().contains(key.lowercase())) {
            tempList.add(school)
        }
    }
    return tempList
}
