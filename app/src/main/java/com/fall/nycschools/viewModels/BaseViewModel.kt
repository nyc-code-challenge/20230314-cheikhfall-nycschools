package com.fall.nycschools.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fall.nycschools.dispatcher.DispatcherProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.plus
import javax.inject.Inject

@HiltViewModel
open class BaseViewModel @Inject constructor(
    dispatcherProvider: DispatcherProvider
) : ViewModel() {

    val scope: CoroutineScope = (viewModelScope + dispatcherProvider.default())
}