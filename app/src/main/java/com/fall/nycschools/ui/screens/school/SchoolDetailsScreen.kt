package com.fall.nycschools.ui.screens.school

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fall.nycschools.R
import com.fall.nycschools.data.models.SatScores
import com.fall.nycschools.data.models.School
import com.fall.nycschools.ui.components.buttons.PrimaryButton
import com.fall.nycschools.ui.components.textFields.LabelAndTextRow
import com.fall.nycschools.ui.screens.error.FullScreenError
import com.fall.nycschools.ui.screens.loader.FullScreenLoadingIndicator
import com.fall.nycschools.viewModels.ScoreUiState

/**
 * This is displaying the selected school details
 */
@Composable
fun SchoolDetailsScreen(
    modifier: Modifier = Modifier,
    navController: NavHostController,
    scoreUiState: ScoreUiState,
    school: School?
) {

    var satScores by remember {
        mutableStateOf<SatScores?>(null)
    }

    Box(modifier = Modifier.fillMaxSize()) {

        Column(
            modifier = modifier
                .fillMaxSize()
                .padding(horizontal = 20.dp)
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                text = school?.schoolName.orEmpty(),
                style = MaterialTheme.typography.h6,
                textAlign = TextAlign.Center
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                text = stringResource(R.string.contact_info),
                style = MaterialTheme.typography.h6,
                textAlign = TextAlign.Center
            )

            LabelAndTextRow(
                label = stringResource(R.string.phone_number),
                text = school?.phoneNumber.orEmpty()
            )
            LabelAndTextRow(
                label = stringResource(R.string.email),
                text = school?.schoolEmail.orEmpty()
            )
            LabelAndTextRow(
                label = stringResource(R.string.website),
                text = school?.website.orEmpty()
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                text = stringResource(R.string.address),
                style = MaterialTheme.typography.h6,
                textAlign = TextAlign.Center
            )

            LabelAndTextRow(
                label = stringResource(R.string.street),
                text = school?.primaryAddressLine1.orEmpty()
            )
            LabelAndTextRow(
                label = stringResource(R.string.city),
                text = school?.city.orEmpty()
            )
            LabelAndTextRow(
                label = stringResource(R.string.zip_code),
                text = school?.zip.orEmpty()
            )
            LabelAndTextRow(
                label = stringResource(R.string.state),
                text = school?.stateCode.orEmpty()
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                text = stringResource(R.string.sat_cores),
                style = MaterialTheme.typography.subtitle1.copy(fontWeight = FontWeight.Bold),
                textAlign = TextAlign.Center
            )

            LabelAndTextRow(
                label = stringResource(R.string.sat_takers),
                text = satScores?.numOfSatTestTakers.orEmpty()
            )

            LabelAndTextRow(
                label = stringResource(R.string.reading_avg_score),
                text = satScores?.satCriticalReadingAvgScore.orEmpty()
            )

            LabelAndTextRow(
                label = stringResource(R.string.math_avg_score),
                text = satScores?.satMathAvgScore.orEmpty()
            )

            LabelAndTextRow(
                label = stringResource(R.string.writing_avg_score),
                text = satScores?.satWritingAvgScore.orEmpty()
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                text = stringResource(R.string.overview),
                style = MaterialTheme.typography.h6,
                textAlign = TextAlign.Center
            )

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 100.dp),
                text = school?.overviewParagraph.orEmpty(),
                style = MaterialTheme.typography.body1,
                textAlign = TextAlign.Justify
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White)
                .padding(horizontal = 16.dp, vertical = 10.dp)
                .align(alignment = Alignment.BottomCenter),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            PrimaryButton(
                modifier = Modifier
                    .fillMaxWidth(),
                text = stringResource(R.string.back),
                onClick = { navController.navigateUp() }
            )
        }
    }

    when (scoreUiState) {
        ScoreUiState.Loading -> {
            FullScreenLoadingIndicator()
        }

        is ScoreUiState.Success -> {
            LaunchedEffect(Unit) {
                satScores = scoreUiState.data
            }
        }

        is ScoreUiState.Error -> {
            FullScreenError(
                errorMessage = stringResource(R.string.general_error_message),
                buttonText = stringResource(R.string.close)
            ) {
                navController.navigateUp()
            }
        }
    }
}
