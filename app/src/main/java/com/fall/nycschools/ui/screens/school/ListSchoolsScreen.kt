package com.fall.nycschools.ui.screens.school

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fall.nycschools.R
import com.fall.nycschools.data.models.School
import com.fall.nycschools.ui.components.rows.SchoolRow
import com.fall.nycschools.ui.components.textFields.SearchOutlineTextField
import com.fall.nycschools.ui.screens.error.FullScreenError
import com.fall.nycschools.ui.screens.loader.FullScreenLoadingIndicator
import com.fall.nycschools.utils.searchSchool
import com.fall.nycschools.viewModels.SchoolUiState

private val listSchoolsResponse = mutableStateOf(listOf<School>())

@Composable
fun ListSchoolsScreen(
    navController: NavHostController,
    schoolUiState: SchoolUiState,
    onClick: (School) -> Unit
) {

    var searchText by remember {
        mutableStateOf("")
    }

    var listSchools by remember {
        mutableStateOf(listOf<School>())
    }

    listSchools = if (searchText.isBlank()) {
        listSchoolsResponse.value
    } else {
        listSchoolsResponse.value.searchSchool(searchText)
    }

    Column(modifier = Modifier.fillMaxSize()) {
        SearchOutlineTextField(
            modifier = Modifier.padding(horizontal = 10.dp),
            onClearClick = { searchText = "" },
            onSearchTextChanged = { searchText = it }
        )
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            contentPadding = PaddingValues(20.dp)
        ) {
            itemsIndexed(listSchools) { index, school ->
                SchoolRow(
                    school = school,
                    number = "${index + 1}",
                    onRowClicked = {
                        onClick.invoke(school)
                    }
                )
            }
        }
    }

    when (schoolUiState) {
        SchoolUiState.Loading -> {
            FullScreenLoadingIndicator()
        }

        is SchoolUiState.Success -> {
            LaunchedEffect(Unit) {
                listSchoolsResponse.value = schoolUiState.data
            }
        }

        is SchoolUiState.Error -> {
            FullScreenError(
                errorMessage = stringResource(R.string.general_error_message),
                buttonText = stringResource(R.string.close)
            ) {
                navController.navigateUp()
            }
        }
    }
}
