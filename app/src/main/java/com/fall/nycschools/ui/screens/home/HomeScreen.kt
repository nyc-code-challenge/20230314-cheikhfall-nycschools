package com.fall.nycschools.ui.screens.home

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fall.nycschools.R
import com.fall.nycschools.navigation.NavScreens
import com.fall.nycschools.ui.components.buttons.PrimaryButton

/**
 * This is the first screen that will launch when the app is started
 */
@Composable
fun HomeScreen(modifier: Modifier = Modifier, navController: NavHostController) {

    Column(
        modifier = modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 20.dp),
            text = stringResource(R.string.welcome_message),
            style = MaterialTheme.typography.h5,
            textAlign = TextAlign.Center
        )

        PrimaryButton(
            text = stringResource(R.string.start),
            onClick = { navController.navigate(NavScreens.ListSchoolsScreen.route) }
        )
    }
}