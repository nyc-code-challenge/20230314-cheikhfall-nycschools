package com.fall.nycschools.di

import com.fall.nycschools.data.api.SchoolApi
import com.fall.nycschools.dispatcher.DefaultDispatcherProvider
import com.fall.nycschools.dispatcher.DispatcherProvider
import com.fall.nycschools.repositories.SchoolRepository
import com.fall.nycschools.utils.Constants
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(ViewModelComponent::class)
object SchoolModule {

    @Provides
    fun provideDispatcherProvider(): DispatcherProvider = DefaultDispatcherProvider()

    // This is providing the retrofit instance
    @Provides
    fun provideSchoolRetrofit(): Retrofit {
        val gson = GsonBuilder().setLenient().create()
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
    }

    // This is providing the api instance
    @Provides
    fun provideSchoolApi(retrofit: Retrofit): SchoolApi {
        return retrofit.create(SchoolApi::class.java)
    }

    // This is providing the SchoolRepository
    @Provides
    fun provideSchoolRepository(schoolApi: SchoolApi): SchoolRepository = SchoolRepository(schoolApi)
}