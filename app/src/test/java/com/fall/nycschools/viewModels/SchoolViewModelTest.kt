package com.fall.nycschools.viewModels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.fall.nycschools.data.models.School
import com.fall.nycschools.repositories.SchoolRepository
import com.fall.nycschools.utils.CoroutineTestRule
import com.fall.nycschools.utils.SchoolApiResponse
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SchoolViewModelTest {

    @get:Rule
    var coroutineTestRule = CoroutineTestRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var schoolRepository: SchoolRepository

    @MockK
    lateinit var school: School

    private lateinit var viewModel: SchoolViewModel
    private val schools = mutableListOf<School>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        schools.add(school)
        viewModel = SchoolViewModel(
            dispatcherProvider = coroutineTestRule.testDispatcherProvider,
            schoolRepository = schoolRepository
        )
    }

    @Test
    fun `should return list schools when the get school api call succeed`() =
        runTest(coroutineTestRule.testDispatcher) {

            coEvery { schoolRepository.getSchools() } returns SchoolApiResponse.Success(schools)

            viewModel.getSchools()

            coVerify { schoolRepository.getSchools() }

            assert(viewModel.schoolUiState.value is SchoolUiState.Success)
        }

    @Test
    fun `should return Error when the get school api call failed`() =
        runTest(coroutineTestRule.testDispatcher) {

            coEvery { schoolRepository.getSchools() } returns SchoolApiResponse.Error(Exception())

            viewModel.getSchools()

            coVerify { schoolRepository.getSchools() }

            assert(viewModel.schoolUiState.value is SchoolUiState.Error)
        }

}